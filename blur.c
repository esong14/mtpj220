#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "ppm_io.h"
#define PI 3.14159

double sq(double input) {
  return input * input;
}

void blur(Image *image, double sigma) {
  int dx, dy;
  // the gaussian grid should hold at least 10*sigma positions
  int n = sigma * 10;
  int image_size = image->cols * image->rows;
  
  // n should not be even
  if (n % 2 == 0) {
    n++;
  }

  // create a gaussian grid
  double gaussian_grid [n][n];
  for (int row = 0; row < n; row++) {
    for (int col = 0; col < n; col++) {
      dx = abs(col - n/2);
      dy = abs(row - n/2);
      gaussian_grid[row][col] = (1.0 / (2.0 * PI * sq(sigma))) * exp( -(sq(dx) + sq(dy)) / (2 * sq(sigma)));
    }
  }

  Pixel *to_be_blurred = (Pixel *) malloc(sizeof(Pixel) * image_size);

  double r, g, b, weight = 0.0;
  int convolve_row, convolve_col;
  // convolve the image
  for (int image_row = 0; image_row < image->rows; image_row++) {
    for (int image_col = 0; image_col < image->cols; image_col++) {
      r = 0.0;
      g = 0.0;
      b = 0.0;
      weight = 0.0;
      for (int gaussian_row = 0; gaussian_row < n; gaussian_row++) {
        for (int gaussian_col = 0; gaussian_col < n; gaussian_col++) {
          convolve_row = image_row - n/2 + gaussian_row;
          convolve_col = image_col - n/2 + gaussian_col;
          // check whether the pixel can be convolved
          if (convolve_row >= 0 && convolve_col >= 0) {
            if (convolve_row < image->rows && convolve_col < image->cols) {
              weight += gaussian_grid[gaussian_row][gaussian_col];
              r += (image->data[convolve_row * image->cols + convolve_col].r) * gaussian_grid[gaussian_row][gaussian_col];
              g += (image->data[convolve_row * image->cols + convolve_col].g) * gaussian_grid[gaussian_row][gaussian_col];
              b += (image->data[convolve_row * image->cols + convolve_col].b) * gaussian_grid[gaussian_row][gaussian_col];
            }
          }
        }
      }
      to_be_blurred[image->cols * image_row + image_col].r = r/weight;
      to_be_blurred[image->cols * image_row + image_col].g = g/weight;
      to_be_blurred[image->cols * image_row + image_col].b = b/weight;
    }
  }

  image->data = to_be_blurred;

}

int check_errors_blur(Image *image, int argc, char *argv[]) {
    
    if (argc != 5) {
      fprintf(stderr, "Incorrect number of arguments to execute blur.\n");
      return 5;
    }

    double sigma;
    if (sscanf(argv[4], "%lf", &sigma) != 1) {
      fprintf(stderr, "Your sigma value must be of type double.\n");
      return 5;
    }

    if (sigma <= 0) {
      fprintf(stderr, "Sigma must be a value positive.\n");
      return 6;
    }

    blur(image, sigma);
    return 0;
    
}

int main() {
    FILE* fp = fopen("nika.ppm", "rb");
    Image * as = read_ppm(fp);
    blur(as, 3);
    fp = fopen("blurred_nika.ppm", "wb");
    write_ppm(fp, as);
    free(as);
    return 0;
}
