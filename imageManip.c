#include "ppm_io.h"
#include <math.h>
#include <assert.h>
#include <string.h>
#define PI 3.14


//clamps the value to range [0,255]
int rangeCol(int value) {
    if(value > 255) {
        value = 255;
    } else if(value < 0) {
        value = 0;
    } 
    return value;

}

//Squares the input
double sq(double input) {
  return input * input;
}

/*
 * Taking an image and an integer value as a parameter,
 * this function brightens the image according to the given value.
 */
void bright(int changeVal, Image * img) {

    //determines the size of the image(number of pixels)
    int size = img->rows * img->cols;

    //for every pixel in the file:
    for(int i = 0; i < size; i++) {
        //add changeVal to all r, g, b value
        img->data[i].r = rangeCol(img->data[i].r + changeVal);
        img->data[i].g = rangeCol(img->data[i].g + changeVal);
        img->data[i].b = rangeCol(img->data[i].b + changeVal);
    }
}

/*
 * Taking an image as a parameter, this function turns the image into
 * grayscale.
 */
void grayscale(Image * img) {

    //determines the size of the image(number of pixels)
    int size = img->rows * img->cols;

    //reading pixels from file and loading into memory
    for(int i = 0; i < size; i++) {
        int intensity = rangeCol(((img->data[i].r)*0.30) + ((img->data[i].g) * 0.59) + ((img->data[i].b) * 0.11));
        img->data[i].r = intensity;
        img->data[i].g = intensity;
        img->data[i].b = intensity;
    }
}

/*
 * Taking an image and 4 values(coordinates on image) as parameters,
 * this function crops the designated area of the image.
 * This function cuts the image(image size will change).
 */
Image * crop(Image * img, int topc, int topr, int bc, int br) {

    //Image to return
    Image * result = malloc(sizeof(Image));
    result->rows = (br-topr);
    result->cols = (bc-topc);

    //allocating memory for pixels in rows
    int size = result->cols * result->rows;
    result->data = (Pixel*) malloc(size*sizeof(Pixel));
    
    int resi = 0;
    for(int row = topr; row < br; row++) {
        for(int col = topc; col < bc; col++) {
            result->data[resi].r = img->data[(img->cols)*row+col].r;
            result->data[resi].g = img->data[(img->cols)*row+col].g;
            result->data[resi].b = img->data[(img->cols)*row+col].b;
            resi++;
        }
    } 
    return result;
}

/*
 * Taking an image and 4 values(coordinates on image) as parameters,
 * this function occludes the designated area of the image given.
 */
Image * occlude(Image * img, int topc, int topr, int bc, int br) {
    
    //for every row in the designated area:
    for(int row = topr; row <= br; row++) {
        //for every pixel in the designated row:
        for(int col = topc; col <= bc; col++) {
            //set the color to black
            img->data[(img->cols)*row+col].r = 0;
            img->data[(img->cols)*row+col].g = 0;
            img->data[(img->cols)*row+col].b = 0;
        }
    } 
    return img;
}

/*
 * Taking an Image and a double value as a parameter,
 * this function applies gaussian blur filter to the
 * image given according to the sigma value.
 */ 
void blur(Image *image, double sigma) {

  int dx, dy;
  // the gaussian grid should hold at least 10*sigma positions
  int n = sigma * 10;
  int image_size = image->cols * image->rows;

  // n should not be even
  if (n % 2 == 0) {
    n++;
  }

  // create a gaussian grid
  double gaussian_grid [n][n];
  int counter = 0;
  for (int row = 0; row < n; row++) {
    for (int col = 0; col < n; col++) {
      dx = abs(col - n/2);
      dy = abs(row - n/2);
      gaussian_grid[row][col] = (1.0 / (2.0 * PI * sq(sigma))) * exp( -(sq(dx) + sq(dy)) / (2.0 * sq(sigma)));
      counter++;
    }
  }

  Pixel *to_be_blurred = (Pixel *) malloc(sizeof(Pixel) * image_size);

  double r, g, b, weight = 0.0;
  int convolve_row, convolve_col;
  // convolve the image
  for (int image_row = 0; image_row < image->rows; image_row++) {
    for (int image_col = 0; image_col < image->cols; image_col++) {
      r = 0.0;
      g = 0.0;
      b = 0.0;
      weight = 0.0;
        for (int gaussian_row = 0; gaussian_row < n; gaussian_row++) {
            for (int gaussian_col = 0; gaussian_col < n; gaussian_col++) {
            convolve_row = image_row - n/2 + gaussian_row;
            convolve_col = image_col - n/2 + gaussian_col;
                // check whether the pixel can be convolved
                if((convolve_row >= 0 && convolve_col >= 0)&&(convolve_row < image->rows && convolve_col < image->cols)){
                    weight += gaussian_grid[gaussian_row][gaussian_col];
                    r += (image->data[convolve_row * image->cols + convolve_col].r) * gaussian_grid[gaussian_row][gaussian_col];
                    g += (image->data[convolve_row * image->cols + convolve_col].g) * gaussian_grid[gaussian_row][gaussian_col];
                    b += (image->data[convolve_row * image->cols + convolve_col].b) * gaussian_grid[gaussian_row][gaussian_col];
                }            
            }
        }
            to_be_blurred[image->cols * image_row + image_col].r = r/weight;
            to_be_blurred[image->cols * image_row + image_col].g = g/weight;
            to_be_blurred[image->cols * image_row + image_col].b = b/weight;
    }
  }
    for (int image_row = 0; image_row < image->rows; image_row++) {
        for (int image_col = 0; image_col < image->cols; image_col++)  {
            image->data[image->cols * image_row + image_col].r = to_be_blurred[image->cols * image_row + image_col].r;
            image->data[image->cols * image_row + image_col].g = to_be_blurred[image->cols * image_row + image_col].g;
            image->data[image->cols * image_row + image_col].b = to_be_blurred[image->cols * image_row + image_col].b;
        }
    }
    free(to_be_blurred);

}

void edges(Image *image, double sigma, double val) {
    //Grayscaling and de-noising image
    grayscale(image);
    blur(image, sigma);
    //size of image
    int size = image->rows * image-> cols;
    //array to store magGrad Values
    double magGradArr[size];

    //Edge Detection Starts here
    //Populating the Magnitude Gradient Array
    //for every row in the interior area
    for(int row = 1; row < image->rows-1; row++) {
        //for every pixel in the row in interior area:
        for(int col = 1; col < image->cols-1; col++) {
            int cur = (image->cols)*row+col;
            double deltaIX = ((double)((image->data[(image->cols)*row+(col+1)].r) - (image->data[(image->cols)*row+(col-1)].r)))/2.0;
            double deltaIY = ((double)((image->data[(image->cols)*(row+1)+col].r) - (image->data[(image->cols)*(row-1)+col].r)))/2.0;
            double magGrad = sqrt(sq(deltaIX)+sq(deltaIY));
            magGradArr[cur] = magGrad;
        }
    }

    //printing the image
    //for every row in the interior area
    for(int row = 1; row < image->rows-1; row++) {
        //for every pixel in the row in interior area:
        for(int col = 1; col < image->cols-1; col++) {
            int k = (image->cols)*row+col;
            if (magGradArr[k] >= val) {
                image->data[k].r = 0;
                image->data[k].g = 0;
                image->data[k].b = 0;
            } else {
                image->data[k].r = 255;
                image->data[k].g = 255;
                image->data[k].b = 255;
            }
        }
    } 
}

/*
 * Gets a command line agrument from the user including
 * input file, output file, command, and appropriate arguments.
 * Manipulates the file accordingly and outputs it to the designated
 * file. Returns the error code.
 */ 
int imageManip(int argc, char *argv[]) {
    if (argc < 3) {
        printf("please specify input and output filenames\n");
        printf("the format for input is: ./project inputfile outputfile Command, and then necessary arguments\n");
        return 1;
    }
    char* inputFile = argv[1];
    char* outputFile = argv[2];
    FILE* fp = fopen(inputFile, "rb");

    //Checks if input file can be opened
    if (fp == NULL) {
        printf("Could not open specified input file.\n");
        return 2;
    }

    //Reads input file into Image
    Image * image = read_ppm(fp);
    fclose(fp);
    //Checks that the file was PPM file (or other reasons for ppm read to return null image)
    if (image == NULL) {
        printf("reading failed, could be improperly formatted PPM file\n");
        return 3;
    }

   /*
    *this block assesses the command and appropriate arguments for validity and 
    *executes the command if appropriate
    *did not separate into functions because doing so would not yield much
    *efficiency in this particular case
    *ex) if separated argc compare statement I will would have to compare it to a val
    *and return the value, so inefficient memory wise too.
    */
    if(strcmp(argv[3], "bright") == 0) {
        if (argc != 5) {
            printf("Incorrect Number of Arguments for bright\n");
            return 5;
        }
        bright(atoi(argv[4]), image);
    } else if (strcmp(argv[3], "gray") == 0) {
        if (argc != 4) {
            printf("Incorrect Number of Arguments for gray\n");
            return 5;
        }
        grayscale(image);
    } else if (strcmp(argv[3], "crop") == 0) {
        int tc = atoi(argv[4]);
        int tr = atoi(argv[5]);
        int bc = atoi(argv[6]);
        int br = atoi(argv[7]);
        if (argc != 8) {
            printf("Incorrect Number of Arguments for crop\n");
            return 5;
        } else if ((tc < 0 || tc < 0 || tc > image->cols || tr > image -> rows)||(bc < 0 || bc < 0 || bc > image->cols || br > image -> rows)) {
            printf("arguments out of range\n");
            return 6;
        }
        image = crop(image, tc, tr, bc, br);
    } else if (strcmp(argv[3], "occlude") == 0) {
        int tc = atoi(argv[4]);
        int tr = atoi(argv[5]);
        int bc = atoi(argv[6]);
        int br = atoi(argv[7]);
        if (argc != 8) {
            printf("Incorrect Number of Arguments for occlude\n");
            return 5;
        } else if ((tc < 0 || tc < 0 || tc > image->cols || tr > image -> rows)||(bc < 0 || bc < 0 || bc > image->cols || br > image -> rows)) {
            printf("arguments out of range\n");
            return 6;
        }
        image = occlude(image, tc, tr, bc, br);
    } else if (strcmp(argv[3], "blur") == 0) {
        if (argc != 5) {
            printf("Incorrect Number of Arguments for blur\n");
            return 5;
        }
        double sigma;
        if (sscanf(argv[4], "%lf", &sigma) != 1) {
            fprintf(stderr, "Your sigma value must be of type double.\n");
            return 5;
        }
        if (sigma <= 0) {
            fprintf(stderr, "Sigma must be a value positive.\n");
            return 6;
        }
        blur(image, sigma);
    } else if (strcmp(argv[3], "edges") == 0) {
       if (argc != 6) {
            printf("Incorrect Number of Arguments for edges\n");
            return 5;
       } 
        double sigma;
        if (sscanf(argv[4], "%lf", &sigma) != 1) {
            printf("Your sigma value must be of type double.\n");
            return 5;
        }
        if (sigma <= 0) {
            printf("Sigma must be a value positive.\n");
            return 6;
        }
        if (atoi(argv[5]) < 0) {
            printf("The threshold value should be positive. \n");
            return 6;
        }
        edges(image, sigma, atoi(argv[5]));   
    } else {
        printf("operation name invalid");
        return 4;
    }
    //open file to write on
    fp = fopen(outputFile, "wb");
    //write the image to file
    write_ppm(fp, image);
    //close the file
    fclose(fp);
    //freeing data separately to resolve indirect loss
    free(image->data);
    //free allocated space for image
    free(image);
    return 0;
}
