#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H

#include "ppm_io.h"


//clamps the value to range [0,255]
int rangeCol(int Value);

//Squares the input
double sq(double input);

/*
 * Taking an image and an integer value as a parameter,
 * this function brightens the image according to the given value.
 */
void bright(int changeVal, Image * img);

/*
 * Taking an image as a parameter, this function turns the image into
 * grayscale.
 */
void grayscale(Image * img);

/*
 * Taking an image and 4 values(coordinates on image) as parameters,
 * this function crops the designated area of the image.
 * This function cuts the image(image size will change).
 */
Image * crop(Image * img, int topc, int topr, int bc, int br);

/*
 * Taking an image and 4 values(coordinates on image) as parameters,
 * this function occludes the designated area of the image given.
 */
Image * occlude(Image * img, int topc, int topr, int bc, int br);

/*
 * Taking an Image and a double value as a parameter,
 * this function applies gaussian blur filter to the
 * image given according to the sigma value.
 */ 
void blur(Image *image, double sigma);

void edges(Image *image, double sigma, double val);

/*
 * Gets a command line agrument from the user including
 * input file, output file, command, and appropriate arguments.
 * Manipulates the file accordingly and outputs it to the designated
 * file. Returns the error code.
 */ 
int imageManip(int argc, char *argv[]);

#endif