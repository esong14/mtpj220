#include <stdio.h>
#include "ppm_io.h"

int main() {
    //testing ppm io
    FILE* fp = fopen("nika.ppm", "rb");
    Image * result = read_ppm(fp);
    fp = fopen("nikacopied.ppm", "wb");
    write_ppm(fp, result);
    return 0;
    free(result);
}