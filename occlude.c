#include "ppm_io.h"

Image * occlude(Image * img, int topc, int topr, int bc, int br) {

    for(int i = topr; i < br; i++) {
        for(int j = topc; j < bc; j++) {
            img->data[(img->cols)*(i-1)+j].r = 0;
            img->data[(img->cols)*(i-1)+j].g = 0;
            img->data[(img->cols)*(i-1)+j].b = 0;
        }
    } 

  return img;
  
}

int main() {
    FILE* fp = fopen("nika.ppm", "rb");
    Image * as = read_ppm(fp);
    as = occlude(as, 250, 280, 530, 370);
    fp = fopen("occludenika.ppm", "wb");
    write_ppm(fp, as);
    free(as);
    return 0;
}