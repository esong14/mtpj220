CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra

project: project.o imageManip.o ppm_io.o
	$(CC) -o project project.o imageManip.o ppm_io.o -lm

project.o: project.c imageManip.h ppm_io.h
	$(CC) $(CFLAGS) -c project.c -lm

imageManip.o: imageManip.c imageManip.h ppm_io.h
	$(CC) $(CFLAGS) -c imageManip.c -lm

ppm_io.o: ppm_io.c ppm_io.h
	$(CC) $(CFLAGS) -c ppm_io.c 

clean:
	rm -f *.o project