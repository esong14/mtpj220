commit 2fcd9e13b58a4398ce5544bb1c1c8b02ac27a831
Author: Eugene Song <esong14@jhu.edu>
Date:   Sat Jun 13 05:11:53 2020 -0400

    resolved blur white line issue

commit 4ec97f8b1c6660f8634fff7e6666813af619ece6
Author: Jang Woo Park <jpark278@jh.edu>
Date:   Sat Jun 13 02:51:41 2020 -0400

    still debugging blur.c

commit 2d4de033f1b2b4704e523ddaeb78bf110bf4b05d
Author: Jang Woo Park <pjwoo3@gmail.com>
Date:   Sat Jun 13 04:27:39 2020 +0000

    blur.c edited online with Bitbucket

commit 47a891c1e770909caab89d2593c526b06a6ea331
Author: Jang Woo Park <pjwoo3@gmail.com>
Date:   Sat Jun 13 03:34:58 2020 +0000

    blur.c edited online with Bitbucket

commit 14ee9b86524e5e7baf681e17fe9abce6f4bca8e4
Author: Jang Woo Park <pjwoo3@gmail.com>
Date:   Sat Jun 13 03:01:57 2020 +0000

    blur.c edited online with Bitbucket

commit 2fb1bcb79d840365cd4b55286663362bee725f20
Author: Eugene Song <esong14@jhu.edu>
Date:   Fri Jun 12 17:38:48 2020 -0400

    Edge Detection Functional, need to check blur function!

commit 704755bfa1a14480e937508f0e35845766b6a01e
Author: Eugene Song <esong14@jhu.edu>
Date:   Thu Jun 11 21:33:58 2020 -0400

    added edges function, not functional yet

commit 809739d23a4eec487b7abbdea21dc39644ffa4be
Author: Eugene Song <esong14@jhu.edu>
Date:   Thu Jun 11 18:01:45 2020 -0400

    simplified code, accomodates for changes in imagemanip

commit 6c99c90ea86ab8429c7fabcf00750366ae1adcff
Author: Eugene Song <esong14@jhu.edu>
Date:   Thu Jun 11 17:58:49 2020 -0400

    created and finished makefile

commit 684fd648f3110d372189c14b6d2d5b92934ec70a
Author: Eugene Song <esong14@jhu.edu>
Date:   Thu Jun 11 17:58:31 2020 -0400

    improved readability of code. also added comments

commit 453c04117bf09c10aa75e30d5cc9ee4925e32d35
Author: Eugene Song <esong14@jhu.edu>
Date:   Thu Jun 11 17:11:19 2020 -0400

    imagemanip header file in progress, project.c created

commit 723b3b514f0af5b90d26c762c764fb9207581732
Author: Eugene Song <esong14@jhu.edu>
Date:   Thu Jun 11 16:41:06 2020 -0400

    imageManip Created, still has warnings

commit 98e8008b1090cc76fa7297c1030d4bb7a12e2ce4
Author: Eugene Song <esong14@jhu.edu>
Date:   Thu Jun 11 15:30:59 2020 -0400

    imageManip.c in progress, currently just combining files

commit 2c06337c2644f1841763101f43475efc7d7cb50e
Author: Eugene Song <esong14@jhu.edu>
Date:   Thu Jun 11 15:09:14 2020 -0400

    fixed blur.c to be fully functional, addressed reference issues

commit 42a0aa0fc10fea390856dee0e73e79e680bed8ab
Author: Eugene Song <esong14@jhu.edu>
Date:   Thu Jun 11 14:54:57 2020 -0400

    resolved variable scope issue of img in grayscale.c

commit 02d45b6f6311e033ae9d5c2fc7e072c701079ed7
Author: Eugene Song <esong14@jhu.edu>
Date:   Thu Jun 11 14:54:00 2020 -0400

    resolved nested function issue'

commit 5926d56d7b92c9fc11272a062525279a3dc7594a
Author: Eugene Song <esong14@jhu.edu>
Date:   Thu Jun 11 14:52:33 2020 -0400

    added clamping function to grayscale.c

commit c03c0c7389a8a514791cc27b722781d41336948e
Author: Jang Woo Park <pjwoo3@gmail.com>
Date:   Thu Jun 11 13:42:44 2020 +0000

    blur.c edited online with Bitbucket

commit a711883edc9af33881f3f5f3a58ed9acc6a423f7
Author: Jang Woo Park <pjwoo3@gmail.com>
Date:   Thu Jun 11 13:42:04 2020 +0000

    blur.c needs debugging

commit 31b88e37e640dd79d758e9182b519eea47a6ed68
Author: Jang Woo Park <jpark278@jh.edu>
Date:   Thu Jun 11 08:45:24 2020 -0400

    still working on it blur.c

commit 86be3cd2c2304eff72415b0cb311cb9957dd6d73
Author: Jang Woo Park <pjwoo3@gmail.com>
Date:   Thu Jun 11 12:30:28 2020 +0000

    blur.c edited online with Bitbucket

commit 3bd3ad13556cdc35fac787da107d6986ed5ae435
Author: Jang Woo Park <pjwoo3@gmail.com>
Date:   Thu Jun 11 11:57:01 2020 +0000

    blur.c edited online with Bitbucket

commit 5662af625aecd8053f2d8a42ebca88e5e5c9d562
Author: Eugene Song <esong14@jhu.edu>
Date:   Thu Jun 11 02:21:05 2020 -0400

    resolved undeclared variable problem in bright.c

commit f86f245b4c9621ea0a2177e7bbfa5c2a9247bdc9
Author: Eugene Song <esong14@jhu.edu>
Date:   Thu Jun 11 02:19:01 2020 -0400

    bright.c algorithm simplified, functional grayscale.c

commit 9ae3b2e5af3a0c20a20e310ae775cedd859108f6
Author: Eugene Song <esong14@jhu.edu>
Date:   Thu Jun 11 02:04:08 2020 -0400

    Removed unnecessary values from occlude.c

commit df448d702778c047b610f317a391627614c0f1ff
Author: Eugene Song <esong14@jhu.edu>
Date:   Thu Jun 11 02:00:00 2020 -0400

    Fully Funttional occlude.c

commit c5bdc1c1de3f1df2a7e911c4f6c73cff8a33841f
Author: Jang Woo Park <pjwoo3@gmail.com>
Date:   Thu Jun 11 03:51:15 2020 +0000

    blur.c in progress

commit 54efd7be1c45565a1781d2f85a6229ce679ff75d
Author: Jang Woo Park <pjwoo3@gmail.com>
Date:   Thu Jun 11 03:37:17 2020 +0000

    blur.c created online with Bitbucket

commit 6bad6fbaa81f40e215025dc4e394931c1d84013b
Author: Eugene Song <esong14@jhu.edu>
Date:   Tue Jun 9 17:53:50 2020 -0400

    functional crop.c and debugged ppm_io.c (fully functional)

commit 9b0f7908a6b7aae9a93e85547774b25c62db4fc8
Author: Eugene Song <esong14@jhu.edu>
Date:   Tue Jun 9 16:19:02 2020 -0400

    debugged to accommodate comments in ppm files

commit 69c02ecafe62356d7a81aabd9ff264e733099d9f
Author: Eugene Song <esong14@jhu.edu>
Date:   Tue Jun 9 15:17:30 2020 -0400

    changed test file to nika, still fully functional

commit 5ef8cd5c8d5b74da5c4e5ab3d575f3df6a6a4b65
Author: Eugene Song <esong14@jhu.edu>
Date:   Tue Jun 9 15:01:29 2020 -0400

    empty crop.c file

commit 935b256da929d025cdcdd308bbfff74e4413ca4e
Author: Eugene Song <esong14@jhu.edu>
Date:   Tue Jun 9 15:00:36 2020 -0400

    fully functional bright.c

commit d0c081f9cb996f51ee7692674dfc35fdba9cc55e
Author: Eugene Song <esong14@jhu.edu>
Date:   Tue Jun 9 14:37:56 2020 -0400

    project.c and bright.c added, they're empty files

commit 0992a4a36aad4f11940f26dd6d4ce251f502acfe
Author: Eugene Song <esong14@jhu.edu>
Date:   Tue Jun 9 14:30:12 2020 -0400

    more visible test file (twitter logo). Still functional

commit 8cb1515c6c32be1f7036eecf3ce053955011a92c
Author: Eugene Song <esong14@jhu.edu>
Date:   Tue Jun 9 14:24:51 2020 -0400

    functional readppm in ppm_io.c
