#include "ppm_io.h"

Image * crop(Image * img, int topc, int topr, int bc, int br) {

    //Image to return
    Image * result = malloc(sizeof(Image));
    result->rows = (br-topr);
    result->cols = (bc-topc);

    //allocating memory for pixels in rows
    int size = result->cols * result->rows;
    result->data = (Pixel*) malloc(size*sizeof(Pixel));
    
    int resi = 0;
    for(int i = topr; i < br; i++) {
        for(int j = topc; j < bc; j++) {
            result->data[resi].r = img->data[(img->cols)*(i-1)+j].r;
            result->data[resi].g = img->data[(img->cols)*(i-1)+j].g;
            result->data[resi].b = img->data[(img->cols)*(i-1)+j].b;
            resi++;
        }
    } 

  return result;
  
}

int main() {
    FILE* fp = fopen("nika.ppm", "rb");
    Image * as = read_ppm(fp);
    as = crop(as, 60, 15, 600, 565);
    fp = fopen("cropnika.ppm", "wb");
    write_ppm(fp, as);
    free(as);
    return 0;
}