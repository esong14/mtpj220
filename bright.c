#include "ppm_io.h"

int rangeCol(int value) {
    if(value > 255) {
        value = 255;
    } else if(value < 0) {
        value = 0;
    } 
    return value;

}

Image * bright(int changeVal, Image * img) {

    int size = img->rows * img->cols;
    //reading pixels from file and loading into memory
    for(int i = 0; i < size; i++) {
        img->data[i].r = rangeCol(img->data[i].r + changeVal);
        img->data[i].g = rangeCol(img->data[i].g + changeVal);
        img->data[i].b = rangeCol(img->data[i].b + changeVal);
    } 

    return img; 
}

int main() {
    FILE* fp = fopen("nika.ppm", "rb");
    Image * img = read_ppm(fp);
    img = bright(100, img);
    fp = fopen("brightnika.ppm", "wb");
    write_ppm(fp, img);
    free(img);
    return 0;
}