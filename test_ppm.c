#include <stdio.h>
#include "ppm_io.h"

int main() {
    FILE* fp = fopen("index.ppm", "rb");
    Image * result = read_ppm(fp);
    fp = fopen("copied.ppm", "wb");
    write_ppm(fp, result);
    return 0;
}