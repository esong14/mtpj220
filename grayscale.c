#include "ppm_io.h"

    int rangeCol(int value) {
        if(value > 255) {
         value = 255;
        } else if(value < 0) {
            value = 0;
        } 
    return value;
   }

Image * grayscale(Image * img) {

    int size = img->rows * img->cols;
    //reading pixels from file and loading into memory
    for(int i = 0; i < size; i++) {
        int intensity = rangeCol(((img->data[i].r)*0.30) + ((img->data[i].g) * 0.59) + ((img->data[i].b) * 0.11));
        img->data[i].r = intensity;
        img->data[i].g = intensity;
        img->data[i].b = intensity;
    }
    return img; 
}

int main() {
    FILE* fp = fopen("nika.ppm", "rb");
    Image * img = read_ppm(fp);
    img = grayscale(img);
    fp = fopen("graynika.ppm", "wb");
    write_ppm(fp, img);
    free(img);
    return 0;
}